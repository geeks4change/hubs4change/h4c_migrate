INTRODUCTION
------------
 
Provides basic import functions for organisations, terms, groups, events and users from CSV. Locations will automatically be attached to organisations.

# EDITORIAL DOCS

## General CSV Settings

* Unicode (UTF-8)
* Field seperator: ,
* Text seperator: "
* Wrap inverted comma around Text
* To separate entities or text lists use ; as separator
* See also example files

## Example CSVs

### Files in example folder

* h4c_terms_import.csv
* h4c_organisations_import.csv
* h4c_events_import.csv
* h4c_users_import.csv
* h4c_users_in_groups.csv

### Notes for h4c_organisations_import.csv

* *required fields*: ID, title, summary, organisation_types
* ID: please fill in unique numbering (e.g. 1, 2,..)
* website: Please use http:// or https:// to have a valid url
* email: Public, only one email address is allowed
* phone: Public, two are allowed (seperated by ;)
* organisations: Fill in ID of organisations this organisation is part of
* opening_hours_short_text: Fill in text, structured opening hours can be added only through UI manually
* location_wheelchair: allowed values: yes, limited or no (single value)
* organisation_types: allowed values: initiative or business or both (seperated by ;)
* topics, business_types, sustainability, tags: enter termns (sperated by ; if multiple values)

# TECHNICAL DOCS

INSTALLATION
------------

* composer install geeks4change/h4c (if necessary)
* drush en h4c_migrate


USEFUL COMMANDS
---------------

* drush ms (migrate-status)
* drush mim (migrate-import) (--limit=100 .. for testing)
* drush mr (migrate-rollback)
* drush mrs (migrate-reset)
* drush mmsg (migrate-messages)
* *Options*: migration-name; --all; --group=migration-group-name

## Reasons for failure on import / Problems you might run into

* Empty lines at the bottom of the csv
* Duplicate IDs (e.g. check with Pivot table in LibreCalc)
* Get ID of group content plugin for h4c_group_content
* Parent Entities must be imported first (organisations / parent terms) otherwise import will stop with dubious errors
* For topics: Remove spaces after delimiter if not present in process plugin


## Setting up the H4C instance to import into

* Deactivate Content Moderation Workflows for nodes to be imported (otherwise nodes will be unpublished after import)
* Deacivate Email Notifications for new content
* Deactivate Group behavior
* taxonomy_machine_name: https://www.drupal.org/project/taxonomy_machine_name "Note: working with Migrate, the reference is no more the 'name' property, but the 'machine_name'."

## Suggestions for import procedure

* Because there are quite a few settings you have to make to your H4C instance, it is usefull to have a dedicated instance/branch only for importing (similar to dev, staging, etc)
* This way, you can re-import or update at any time and simply transfer the new db to dev or staging (don't forget to drush cim after db transfer)

MIGRATING ORGANISATIONS AND TERMS
---------------

### Procedure

* Copy files from ./example/ to docroot/sites/default/files/migration/
* Fill columns with data to be imported
* Important: Organisations which are referenced as Parent Organisations must be first in the csv file (h4c_organisation is referencing itself) ! Otherwise h4c_group_content will throw errors!
* Don't delete any columns, column count is important for field matching

## Migration Groups

Each migration file can also be run individually if the dependencies are met. 

### h4c_general
 
*  Multilingual Terms

###  h4c_organisation_import

* Organisations (with paragraphs), Locations (sourced from organisations), Groups, Group content, Image
* Locations are reusable
* For each organisation a group will be created. Each Organisation is automatically part of the corresponding group.

### h4c_event_import

* Events (with paragraphs), Locations, Image
* Locations are not reusable

### h4c_user_import

* Imports users
* Imports group membership (needs csv h4c_users_in_groups_import.csv)

### h4c_campaign_import

* h4c_campaign imports various node types needed for h4c_campaign, not the campaign nodes themselves (needs to be clarified)
* needs customizations, eg for matching of terms and node type names
* 

## Geocode addresses and maintain existing coordinates using --update (Needs testing)

* The Google API key must be entered and "Use SSL" enabled: /admin/config/system/geocoder
* field_location_geofield is geocoded from field_location_address (only if API key is entered here as well: /admin/config/system/geocoder)
* Import Locations
* Turn off geocoding in field_location_geofield in /admin/structure/types/manage/location/fields/node.location.field_location_geofield from address
* Import --update locations so that imported coordinates in field_location_geofield are maintained and not overriden from address geocoding

## Import from kvm

- https://editor.swagger.io -> File -> Import URL -> https://raw.githubusercontent.com/slowtec/openfairdb/master/openapi.yaml - unten (über Schemas) auf Export, dort die Koordinaten der Schweiz eingeben und auf "Try it out"
- Spalten matchen
- Adresse zusammen setzen: `=I2&", "&J2&", "&" "&K2`
- Location Name ausfüllen
- Tags: "," durch ";" ersetzen
- Business und Initiative klein schreiben

## Further reading:

- [Adding Extra Source Properties in Drupal Migrations | agaric.coop](https://agaric.coop/blog/adding-extra-source-properties-drupal-migrations?utm_source=drupal-newsletter&utm_medium=email&utm_campaign=drupal-newsletter-20190307)
- [Entity Import: A User Interface for Drupal 8 Migrations | atendesigngroup.com](https://atendesigngroup.com/blog/entity-import-user-interface-drupal-8-migrations), [module page on d.org](https://www.drupal.org/project/entity_import)
